package jp.mercari.api.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Item {
	private Long itemId;
	private Long catId;
	private String itemName;
	private byte[] image;
	private int price;
	private Long staId;
	private Long exhibitorId;
	private Long buyerId;
	private int finish;
	private String registerDate;
	private String updatedDate;
	private int unconfirmed;
	public Long getItemId() {
		return itemId;
	}
	public void setItemId(Long itemId) {
		this.itemId = itemId;
	}
	public Long getCatId() {
		return catId;
	}
	public void setCatId(Long catId) {
		this.catId = catId;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public byte[] getImage() {
		return image;
	}
	public void setImage(byte[] image) {
		this.image = image;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public Long getStaId() {
		return staId;
	}
	public void setStaId(Long staId) {
		this.staId = staId;
	}
	public Long getExhibitorId() {
		return exhibitorId;
	}
	public void setExhibitorId(Long exhibitorId) {
		this.exhibitorId = exhibitorId;
	}
	public Long getBuyerId() {
		return buyerId;
	}
	public void setBuyerId(Long buyerId) {
		this.buyerId = buyerId;
	}
	public int getFinish() {
		return finish;
	}
	public void setFinish(int finish) {
		this.finish = finish;
	}
	public String getRegisterDate() {
		return registerDate;
	}
	public void setRegisterDate(String registerDate) {
		this.registerDate = registerDate;
	}
	public String getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}
	public int getUnconfirmed() {
		return unconfirmed;
	}
	public void setUnconfirmed(int unconfirmed) {
		this.unconfirmed = unconfirmed;
	}
	
	

}
