package jp.mercari.api.configuration;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(DataSourceConfigurationProperties.class)
public class DataSourceConfiguration {
	
	@Autowired
	private DataSourceConfigurationProperties properties;

	@Bean
	public DataSource dataSource() {
		BasicDataSource dataSource = new BasicDataSource();

		// JDBCドライバ指�?
		dataSource.setDriverClassName(this.properties.getDriverClassName());

		// 接続情報
		dataSource.setUrl(this.properties.getUrl());
		dataSource.setUsername(this.properties.getUsername());
		dataSource.setPassword(this.properties.getPassword());

		// コネクション数設�?
		dataSource.setInitialSize(this.properties.getInitialSize());
		dataSource.setMaxIdle(this.properties.getMaxIdle());
		dataSource.setMinIdle(this.properties.getMinIdle());

		return dataSource;
	}

}
